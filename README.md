# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) TS template.

To start the app, you just need to install all dependencies over npm and then run `npm start`!

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


## Additional Info

- I chose TailwindCSS as it allowed me to quickly build this simple custom design. I find it a great fit for such a small project but usually I would
  combine it with styled components, as it makes code reading easier.
- I added also some tests, to showcase a bit my skills in them. Usually in a "real" project situation, there would be some testing strategy
  on which kind of test / test tools to emphasize. I had hoped to also write some tests with cypress, unfortunately due to
  https://github.com/cypress-io/cypress/issues/21381 (as it seems some incompatibility with the newest React version)
  I wasn't able to provide you with that.
- I preferred the Redux path instead of GraphQL, as I find that setting the app up for GraphQL needs a bit more effort, and the time was limited.
If the project scope was different, we could weigh other factors additionaly.
- If I had a bit more time? I would actually use styled components, as I mentioned above. I would maybe use GraphQL depending on the app needs (currently we know nothing about its longevity, scope etc.).
  I would also add more tests. Other than that, since the scope is an unknown, the project makes sense as it is.
