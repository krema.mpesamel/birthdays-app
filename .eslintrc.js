/* eslint-env node */
// Allowing node syntax in this file, as an alternative to globally specifying our environment as node
module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'react-hooks'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-hooks/recommended',
    'prettier',
  ],
  env: {
    browser: true,
  },
  globals: {
    process: true,
  },
};
