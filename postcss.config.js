/* eslint-env node */
// Allowing node syntax in this file, as an alternative to globally specifying our environment as node
module.exports = {
  plugins: {
    tailwindcss: {},
    autoprefixer: {},
  },
};
