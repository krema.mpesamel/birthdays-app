import axios from 'axios';
import { RANDOM_USERS_REST_URL } from "../constants";

export const axiosInstance = axios.create({
  baseURL: RANDOM_USERS_REST_URL,
  timeout: 10000,
  headers: { 'Content-Type': 'application/json' },
});
