import * as React from 'react';
import { Routes } from 'react-router-dom';

import { Navigate, Route } from 'react-router-dom';
import { paths } from './paths';
import UsersOverview from "../../components/views/UsersOverview/UsersOverview";

function ApplicationRoutes(): JSX.Element {
  return (
    <Routes>
      {/* DEFAULT REDIRECTING */}
      <Route
        path="/"
        element={<Navigate to={paths.users.overview} />}
      ></Route>

      <Route path="/users/" element={<UsersOverview />} />
    </Routes>
  );
}

export default ApplicationRoutes;
