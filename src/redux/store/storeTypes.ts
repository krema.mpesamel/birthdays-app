import { User } from "../../models/User";

export type UsersState = {
  users: User[];
  isUsersLoading: boolean;
  usersError: string | undefined;
};
