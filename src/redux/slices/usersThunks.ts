import { createAsyncThunk } from '@reduxjs/toolkit';
import { axiosInstance } from '../../api/axiosConfig';

const numberOfResults = 1000;
const seed = 'chalkboard';
const inc = 'name,dob';

export const fetchRandomUsers = createAsyncThunk(
  'randomUsers/from',
  async () => {
    const response = await axiosInstance.get(
      `?results=${numberOfResults}&seed=${seed}&inc=${inc}`
    );
    return response.data;
  }
);
