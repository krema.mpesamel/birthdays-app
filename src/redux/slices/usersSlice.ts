import { UsersState } from '../store/storeTypes';
import { createSlice } from '@reduxjs/toolkit';
import { fetchRandomUsers } from "./usersThunks";
import { RootState } from "../store/store";

const initialState: UsersState = {
  users: [],
  isUsersLoading: false,
  usersError: undefined
};

export const usersSlice = createSlice({
  name: 'user',
  initialState,
  // The `slices` field lets us define slices and generate associated actions
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchRandomUsers.pending, (state) => {
        state.isUsersLoading = true;
        state.usersError = undefined;
      })
      .addCase(fetchRandomUsers.fulfilled, (state, action) => {
        state.users = action.payload?.results;
        state.isUsersLoading = false;
      })
      .addCase(fetchRandomUsers.rejected, (state, action) => {
        state.isUsersLoading = false;
        state.usersError = action.error.message;
      });
  },
});

export const selectAllUsers = (state: RootState) => state.users.users;
export const selectIsUsersLoading = (state: RootState) =>
  state.users.isUsersLoading;
export const selectUsersError = (state: RootState) =>
  state.users.usersError;

export default usersSlice.reducer;
