type Props = {
  errorMessage: string;
};

function GeneralErrorMessage({ errorMessage }: Props) {
  return (
    <div className="flex">
      <div className="m-auto">
        <div className="flex flex-col gap-y-2">
          <h1 className="text-center text-4xl text-gray-600">
            Something unexpected happened
          </h1>
          <h2 className="text-center txt-2xl">
            {errorMessage}
          </h2>
        </div>
      </div>
    </div>
  );
}

export default GeneralErrorMessage;
