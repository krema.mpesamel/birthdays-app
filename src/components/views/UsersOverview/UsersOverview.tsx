import { selectAllUsers, selectIsUsersLoading, selectUsersError } from "../../../redux/slices/usersSlice";
import { useAppDispatch, useAppSelector } from "../../../redux/hooks";
import { useEffect } from "react";
import { fetchRandomUsers } from "../../../redux/slices/usersThunks";
import GeneralErrorMessage from "../../buildingBlocks/ErrorMessage/GeneralErrorMessage";
import LoadingIcon from "../../buildingBlocks/LoadingIcon/LoadingIcon";
import { User } from "../../../models/User";


function UsersOverview() {

  const dispatch = useAppDispatch();

  const allUsers = useAppSelector(selectAllUsers);
  const isLoadingUsers = useAppSelector(selectIsUsersLoading);
  const usersError = useAppSelector(selectUsersError);

  useEffect(() => {
    dispatch(fetchRandomUsers());
  }, [dispatch]);

  if (usersError) {
    return <GeneralErrorMessage errorMessage={usersError} />;
  }

  if (isLoadingUsers) {
    return <LoadingIcon />;
  }

  return (
    <ul>
      {allUsers?.map((user: User, index: number) => <li key={index}>{user?.name.first}</li>)}
    </ul>
  );
}

export default UsersOverview;