import { UserDateOfBirthday } from './UserDateOfBirthday';
import { UserName } from './UserName';

export type User = {
  name: UserName;
  dob: UserDateOfBirthday;
};
