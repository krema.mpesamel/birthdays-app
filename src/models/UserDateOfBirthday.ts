export type UserDateOfBirthday = {
  date: Date;
  age: number;
};
